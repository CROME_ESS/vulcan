#include "mainwindow.h"
#include "ui_mainwindow.h"


#include <QSettings>
#include <QCheckBox>
#include "cromeplot.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setWindowTitle("VULCAN (ICD v5.0)");

    setupWidgets();

    configFile = "config.ini";

    loadSettings();
    ui->cromePlot->setPlotType(0);
    ui->cromePlotRT->setPlotType(1);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::addHistCheckboxes()
{
    checkboxes.insert(0, ui->checkBoxAvg);
    ui->checkBoxAvg->setText("Value Avg");

    checkboxes.insert(1, ui->checkBoxValue);
    ui->checkBoxValue->setText("Value");

    checkboxes.insert(2, ui->checkBoxIntegral1);
    ui->checkBoxIntegral1->setText("Integral1");

    checkboxes.insert(3, ui->checkBoxIntegral2);
    ui->checkBoxIntegral2->setText("Integral2");

    checkboxes.insert(4, ui->checkBoxMin);
    ui->checkBoxMin->setText("Value Min");

    checkboxes.insert(5, ui->checkBoxMax);
    ui->checkBoxMax->setText("Value Max");

    checkboxes.insert(6, ui->checkBoxTemp);
    ui->checkBoxTemp->setText("Temperature");


    checkboxes.insert(7, ui->checkBoxHumidity);
    ui->checkBoxHumidity->setText("Humidity");

    checkboxes.insert(8, ui->checkBoxHighVoltage);
    ui->checkBoxHighVoltage->setText("High Voltage");
}

void MainWindow::addRTCheckBoxes()
{
    checkboxesRT.insert(0, ui->checkBoxRawRT);
    ui->checkBoxRawRT->setText("Value Raw");

    checkboxesRT.insert(1, ui->checkBoxBaseRT);
    ui->checkBoxBaseRT->setText("Value Base");

    checkboxesRT.insert(2, ui->checkBoxAvgRT);
    ui->checkBoxAvgRT->setText("Value Avg");

    checkboxesRT.insert(3, ui->checkBoxTemp1RT);
    ui->checkBoxTemp1RT->setText("Temperature 1");

    checkboxesRT.insert(4, ui->checkBoxTemp2RT);
    ui->checkBoxTemp2RT->setText("Temperature 2");

    checkboxesRT.insert(5, ui->checkBoxTempCaseRT);
    ui->checkBoxTempCaseRT->setText("Temperature Case");

    checkboxesRT.insert(6, ui->checkBoxHumidityRT);
    ui->checkBoxHumidityRT->setText("Humidity");

    checkboxesRT.insert(7, ui->checkBoxMinRT);
    ui->checkBoxMinRT->setText("Min");

    checkboxesRT.insert(8, ui->checkBoxMaxRT);
    ui->checkBoxMaxRT->setText("Max");

    checkboxesRT.insert(9, ui->checkBoxHumidityRT);
    ui->checkBoxInt1RT->setText("Integral1");

    checkboxesRT.insert(10, ui->checkBoxHumidityRT);
    ui->checkBoxInt2RT->setText("Integral2");

    ui->labelElapsedTimes->setText("Elapsed Times [1,2]");
}

void MainWindow::setupWidgets()
{
    ui->groupBoxSCP->setTitle("Download files");

    ui->labelUser->setText("Username");
    ui->labelPassword->setText("Password");
    ui->labelAddress->setText("IP Address");
    ui->labelPorts->setText("Ports");
    ui->labelPath->setText("Data/events parent DIR");

    ui->labelProgress->setText("Progress");
    ui->labelWorkdir->setText("Working directory");

    ui->pushButtonDownload->setText("Download");
    ui->pushButtonChange->setText("Select DIR");

    connect(ui->pushButtonDownload, &QPushButton::clicked, this, &MainWindow::downloadFiles);
    connect(ui->pushButtonConfirm, &QPushButton::clicked, this, &MainWindow::confirmTimestamps);
    connect(ui->pushButtonCSV, &QPushButton::clicked, this, &MainWindow::prepareCSVRequest);
    connect(ui->pushButtonChange, &QPushButton::clicked, this, &MainWindow::selectDirectory);

    connect(ui->radioButtonData, &QRadioButton::clicked, [this](){emit signalRequestDataTimestamps();});
    connect(ui->radioButtonEvents, &QRadioButton::clicked, [this](){emit signalRequestEventTimestamps();});


    ui->labelVersion1->setText("VULCAN (ICD v5.0)");
    ui->labelVersion2->setText("VULCAN (ICD v5.0)");

    ui->groupBoxTimestamps->setTitle("Timestamps selection");
    ui->labelFirstTimestamp->setText("First available");
    ui->labelLastTimestamp->setText("Last Available");

    ui->radioButtonData->setText("Histdata");
    ui->radioButtonEvents->setText("Histevent");
    ui->radioButtonData->setChecked(true);

    ui->labelFromYear->setText("Year");
    ui->labelFromMonth->setText("Month");
    ui->labelFromDay->setText("Day");
    ui->labelFromHour->setText("Hour");
    ui->labelFromMinute->setText("Minute");

    ui->labelToYear->setText("Year");
    ui->labelToMonth->setText("Month");
    ui->labelToDay->setText("Day");
    ui->labelToHour->setText("Hour");
    ui->labelToMinute->setText("Minute");

    ui->labelDecimation->setText("Use each Nth sample:");
    ui->spinDecimation->setMinimum(1);
    ui->spinDecimation->setMaximum(60*60*1000);

    ui->pushButtonConfirm->setText("Confirm Timestamps");
    ui->pushButtonCSV->setText("CSV");

    ui->tabPlots->setCurrentIndex(0);

    ui->tabWidgetHistrecords->layout()->addWidget(&hw);
    ui->tabWidgetEventrecords->layout()->addWidget(&ew);
    ui->tabWidgetStatus->layout()->addWidget(&sw);
    ui->tabWidgetLogging->layout()->addWidget(&lw);

    ui->tabPlots->setTabText(0, "Hist Plot");
    ui->tabPlots->setTabText(1, "Realtime Plot");
    ui->tabPlots->setTabText(2, "Realtime Status");
    ui->tabPlots->setTabText(3, "HistData");
    ui->tabPlots->setTabText(4, "HistEvents");
    ui->tabPlots->setTabText(5, "Logging");

    addHistCheckboxes();
    addRTCheckBoxes();



    foreach(QCheckBox* box, checkboxes) {
        connect(box, &QCheckBox::stateChanged, this, &MainWindow::plotsSelected);
    }

    foreach(QCheckBox* box, checkboxesRT) {
        connect(box, &QCheckBox::stateChanged, this, &MainWindow::plotsSelected);
    }

    ui->groupBoxY1->setTitle("Y1 Limits");
    ui->pushButtonLimitsY1->setText("Set");
    connect(ui->pushButtonLimitsY1, &QPushButton::clicked, this, &MainWindow::setLimitsY1);


    ui->groupBoxY2->setTitle("Y2 Limits");
    ui->pushButtonLimitsY2->setText("Set");
    connect(ui->pushButtonLimitsY2, &QPushButton::clicked, this, &MainWindow::setLimitsY2);



    ui->pushButtonFitX->setText("Fit X axis");
    connect(ui->pushButtonFitX, &QPushButton::clicked, this, &MainWindow::rescaleX);

    ui->pushButtonFitY->setText("Fit Y axis");
    connect(ui->pushButtonFitY, &QPushButton::clicked, this, &MainWindow::rescaleY);

    ui->pushButtonRescale->setText("Rescale plot");
    connect(ui->pushButtonRescale, &QPushButton::clicked, this, &MainWindow::rescale);


    ui->groupBoxY1RT->setTitle("Y1 Limits");
    ui->pushButtonLimitsY1RT->setText("Set");
    connect(ui->pushButtonLimitsY1RT, &QPushButton::clicked, this, &MainWindow::setLimitsY1RT);

    ui->groupBoxY2RT->setTitle("Y2 Limits");
    ui->pushButtonLimitsY2RT->setText("Set");
    connect(ui->pushButtonLimitsY2RT, &QPushButton::clicked, this, &MainWindow::setLimitsY2RT);

    ui->pushButtonStartRT->setText("Start Realtime Plot");
    connect(ui->pushButtonStartRT, &QPushButton::clicked, this, &MainWindow::startRealtimePlot);

    ui->pushButtonCsvRT->setText("Export CSV");
    connect(ui->pushButtonCsvRT, &QPushButton::clicked, this, &MainWindow::prepareRealtimeCSVRequest);

    ui->pushButtonClearRT->setText("Clear");
    connect(ui->pushButtonClearRT, &QPushButton::clicked, this, &MainWindow::clearRealtimePlot);

    ui->pushButtonFitXRT->setText("Fit X axis");
    connect(ui->pushButtonFitXRT, &QPushButton::clicked, this, &MainWindow::rescaleXRT);

    ui->pushButtonFitYRT->setText("Fit Y axis");
    connect(ui->pushButtonFitYRT, &QPushButton::clicked, this, &MainWindow::rescaleYRT);

    ui->pushButtonRescaleRT->setText("Rescale plot");
    connect(ui->pushButtonRescaleRT, &QPushButton::clicked, this, &MainWindow::rescaleRT);


    ui->pushButtonAttach->setText("Attach / Detach");
    connect(ui->pushButtonAttach, &QPushButton::clicked, this, &MainWindow::manipulatePlot);

    ui->pushButtonAttachRT->setText("Attach / Detach");
    connect(ui->pushButtonAttachRT, &QPushButton::clicked, this, &MainWindow::manipulatePlotRT);

    ui->checkBoxLegend->setText("Legend");
    connect(ui->checkBoxLegend, &QCheckBox::clicked, ui->cromePlot, &CromePlot::showLegend);

    ui->checkBoxLegendRT->setText("Legend");
    connect(ui->checkBoxLegendRT, &QCheckBox::clicked, ui->cromePlotRT, &CromePlot::showLegend);

}

void MainWindow::setupSpinboxes()
{
    QVector<QDoubleSpinBox*> spinboxList;
    spinboxList.append(ui->spinFromYear);
    spinboxList.append(ui->spinFromMonth);
    spinboxList.append(ui->spinFromDay);
    spinboxList.append(ui->spinFromHour);
    spinboxList.append(ui->spinFromMinute);

    spinboxList.append(ui->spinToYear);
    spinboxList.append(ui->spinToMonth);
    spinboxList.append(ui->spinToDay);
    spinboxList.append(ui->spinToHour);
    spinboxList.append(ui->spinToMinute);

    foreach (QDoubleSpinBox* spinbox, spinboxList) {
        QObject::connect(spinbox, static_cast<void (QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged), this, &MainWindow::verifyTimestamps);

    }
}

void MainWindow::downloadFiles()
{
    QString user, password, address, path;

    user = ui->lineEditUser->text();
    password = ui->lineEditPassword->text();
    address = ui->lineEditAddress->text();
    path = ui->lineEditPath->text();

    ui->progressBar->setMaximum(0);
    ui->progressBar->setMinimum(100);

    ui->progressBar->setValue(0);

    QMap<QString, QString> args;
    args.insert("user", user);
    args.insert("password", password);
    args.insert("address", address);
    args.insert("path", path);

    saveSettings();

    emit scpRun(args);
}

void MainWindow::selectDirectory()
{
    QString dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                                 lastWorkdir,
                                                 QFileDialog::ShowDirsOnly
                                                 | QFileDialog::DontResolveSymlinks);

    ui->lineEditCurrent->setText(dir);
    ui->radioButtonData->setChecked(true);
    emit signalRequestDirectoryLoad(dir);
}

void MainWindow::updateProgressBar(quint64 progress)
{
    ui->progressBar->setMinimum(0);
    ui->progressBar->setMaximum(100);

    ui->progressBar->setValue(progress);
}

void MainWindow::setMinProgressBar(quint64 minimum)
{
    ui->progressBar->setMinimum(minimum);
}

void MainWindow::setMaxProgressBar(quint64 maximum)
{
    ui->progressBar->setMaximum(maximum);
}

void MainWindow::scpResult(int code, QString currentDir)
{
    if(code == 0){
        ui->lineEditCurrent->setText(currentDir);
    }
}

void MainWindow::setTimestamps(CromeDateTime fromDate, CromeDateTime toDate)
{

    ui->spinFirstTimestamp->setMinimum(fromDate.getTimestamp());
    ui->spinFirstTimestamp->setMaximum(toDate.getTimestamp());
    ui->spinFirstTimestamp->setValue(fromDate.getTimestamp());

    ui->spinLastTimestamp->setMinimum(fromDate.getTimestamp());
    ui->spinLastTimestamp->setMaximum(toDate.getTimestamp());
    ui->spinLastTimestamp->setValue(toDate.getTimestamp());

    ui->lineFirstDatetime->setText(fromDate.getReadableDate());
    ui->lineLastDatetime->setText(toDate.getReadableDate());

    ui->spinFromYear->setMinimum(fromDate.getYear());
    ui->spinFromYear->setMaximum(toDate.getYear());
    ui->spinFromYear->setValue(fromDate.getYear());

    ui->spinFromMonth->setMinimum(1);
    ui->spinFromMonth->setMaximum(12);
    ui->spinFromMonth->setValue(fromDate.getMonth());

    ui->spinFromDay->setMinimum(1);
    ui->spinFromDay->setMaximum(31);
    ui->spinFromDay->setValue(fromDate.getDay());

    ui->spinFromHour->setMinimum(0);
    ui->spinFromHour->setMaximum(23);
    ui->spinFromHour->setValue(fromDate.getHour());

    ui->spinFromMinute->setMinimum(0);
    ui->spinFromMinute->setMaximum(59);
    ui->spinFromMinute->setValue(fromDate.getMinute());


    ui->spinToYear->setMinimum(fromDate.getYear());
    ui->spinToYear->setMaximum(toDate.getYear());
    ui->spinToYear->setValue(toDate.getYear());

    ui->spinToMonth->setMinimum(1);
    ui->spinToMonth->setMaximum(12);
    ui->spinToMonth->setValue(toDate.getMonth());

    ui->spinToDay->setMinimum(1);
    ui->spinToDay->setMaximum(31);
    ui->spinToDay->setValue(toDate.getDay());

    ui->spinToHour->setMinimum(0);
    ui->spinToHour->setMaximum(23);
    ui->spinToHour->setValue(toDate.getHour());

    ui->spinToMinute->setMinimum(0);
    ui->spinToMinute->setMaximum(59);
    ui->spinToMinute->setValue(toDate.getMinute());

    setupSpinboxes();

}

void MainWindow::setDataTimestamps(CromeDateTime first, CromeDateTime last)
{
    if(ui->radioButtonData->isChecked())
    {
        setTimestamps(first, last);
    }
}

void MainWindow::setEventTimestamps(CromeDateTime first, CromeDateTime last)
{
    if(ui->radioButtonEvents->isChecked())
    {
         setTimestamps(first, last);
    }
}

void MainWindow::confirmTimestamps()
{
    QPair<quint64, quint64> stamps = readInputTimestamps();
    quint64 decimation = ui->spinDecimation->value();
    saveSettings();

    if(ui->radioButtonData->isChecked()){
        emit signalConfirmTimestampsData(stamps.first, stamps.second, decimation);
    }

    if(ui->radioButtonEvents->isChecked()){
        emit signalConfirmTimestampsEvent(stamps.first, stamps.second, decimation);
    }

}

void MainWindow::prepareCSVRequest()
{
    QPair<quint64, quint64> stamps = readInputTimestamps();
    quint64 decimation = ui->spinDecimation->value();
    saveSettings();


    QString filename = QFileDialog::getSaveFileName(this, "Save CSV", QDir::homePath(), tr("CSV files (*.csv)"));

    if(filename.isEmpty() == 0) {
        emit signalGetCSV(stamps.first, stamps.second, decimation, filename);
    }

}

void MainWindow::prepareRealtimeCSVRequest()
{
    QString filename = QFileDialog::getSaveFileName(this, "Save Realtime CSV", QDir::homePath(), tr("CSV files (*.csv)"));

    if(filename.isEmpty() == 0) {
        emit signalGetRealtimeCSV(filename);
    }

}


void MainWindow::verifyTimestamps()
{

    QPair<quint64, quint64> stamps = readInputTimestamps();

    quint64 timestampFrom = stamps.first;
    quint64 timestampTo = stamps.second;

    quint64 firstPossibleTimestamp = ui->spinFirstTimestamp->value();
    quint64 lastPossibleTimestamp  = ui->spinLastTimestamp->value();

    bool result = (timestampFrom<=lastPossibleTimestamp)&&(timestampTo>=firstPossibleTimestamp);

    ui->pushButtonConfirm->setEnabled(result);

}

QPair<quint64,quint64> MainWindow::readInputTimestamps()
{
    quint64 year    = ui->spinFromYear->value();
    quint64 month   = ui->spinFromMonth->value();
    quint64 day     = ui->spinFromDay->value();
    quint64 hour    = ui->spinFromHour->value();
    quint64 minute  = ui->spinFromMinute->value();

    quint64 timestampFrom = CromeDateTime::dateToTimestamp(year, month, day, hour, minute);


    year    = ui->spinToYear->value();
    month   = ui->spinToMonth->value();
    day     = ui->spinToDay->value();
    hour    = ui->spinToHour->value();
    minute  = ui->spinToMinute->value();

    quint64 timestampTo = CromeDateTime::dateToTimestamp(year, month, day, hour, minute);

    return QPair<quint64,quint64>(timestampFrom, timestampTo);
}

void MainWindow::receiveDataRecords(QVector<MeasObject> records, QVector<QPair<QString, romulus_hist_data_t>> histdata, QVector<QPair<QString, romulus_hist_event_t>> histevents)
{
    if(ui->radioButtonData->isChecked()) {
        if(!records.isEmpty())
        {
            ui->cromePlot->updatePlotHist(records);
        }

        hw.addData(histdata);
    }
    else {
        ew.addEvents(histevents);
    }
}

void MainWindow::plotsSelected()
{
    QMap<int, bool> checkboxesSelection;
    QMap<int, bool> checkboxesSelectionRT;

    for(int i = 0; i < checkboxes.size(); ++i) {
        checkboxesSelection.insert(i, checkboxes[i]->isChecked());


    }
    for(int i = 0; i < checkboxesRT.size(); ++i) {
        checkboxesSelectionRT.insert(i, checkboxesRT[i]->isChecked());
    }

    ui->cromePlot->checkPlotsVisibility(checkboxesSelection);
    ui->cromePlotRT->checkPlotsVisibility(checkboxesSelectionRT);
}

void MainWindow::rescale()
{
    ui->cromePlot->rescaleOriginal();
}

void MainWindow::rescaleX()
{
    ui->cromePlot->rescaleXAxis();
}

void MainWindow::rescaleY()
{
    ui->cromePlot->rescaleYAxis();
}

void MainWindow::rescaleRT()
{
    ui->cromePlotRT->rescaleOriginal();
}

void MainWindow::rescaleXRT()
{
    ui->cromePlotRT->rescaleXAxis();
}

void MainWindow::rescaleYRT()
{
    ui->cromePlotRT->rescaleYAxis();
}

void MainWindow::startRealtimePlot()
{
    QString ip = ui->lineEditAddress->text();
    QString portNC = ui->lineEditPort2->text();
    QString arguments = ip+":"+portNC;

    emit signalStartRealtimePlot(QStringList() << arguments);
}

void MainWindow::clearRealtimePlot()
{
    ui->cromePlotRT->clearDatapoints();
}

void MainWindow::realtimeDataSlot(MeasObject record)
{
    ui->cromePlotRT->realtimeDataSlot(record);

    ui->lineValueRaw->setText(QString::number(record.getValueRaw()));
    ui->lineValueBase->setText(QString::number(record.getValueBase()));
    ui->lineValueAvg->setText(QString::number(record.getValueAvg()));

    ui->lineTemp1->setText(QString::number(record.getTemperature1()));
    ui->lineTemp2->setText(QString::number(record.getTemperature2()));
    ui->lineTempCase->setText(QString::number(record.getTemperatureCase()));

    ui->lineHumidity->setText(QString::number(record.getHumidity()));
    ui->lineInt1->setText(QString::number(record.getIntegral1()));
    ui->lineInt2->setText(QString::number(record.getIntegral2()));

    ui->lineMin->setText(QString::number(record.getMinValue()));
    ui->lineMax->setText(QString::number(record.getMaxValue()));

    ui->lineElapsedTime1->setText(QString::number(record.getElapsedTime1()));
    ui->lineElapsedTime2->setText(QString::number(record.getElapsedTime2()));

    sw.realtimeStatus(record);

}

void MainWindow::manipulatePlot()
{
    if(detached){
        ui->widgetHist->setWindowFlags(Qt::Widget);
        ui->tabWidgetHistPlot->layout()->addWidget(ui->widgetHist);
        detached = false;
    }
    else {

        detached = true;
        ui->widgetHist->setWindowFlags(Qt::Window | Qt::CustomizeWindowHint | Qt::WindowTitleHint | Qt::WindowMinimizeButtonHint);
        ui->widgetHist->showMaximized();
    }
}

void MainWindow::manipulatePlotRT()
{
    if(detachedRT){
        ui->widgetRT->setWindowFlags(Qt::Widget);
        ui->tabRealtimePlot->layout()->addWidget(ui->widgetRT);
        detachedRT = false;
    }
    else {

        detachedRT = true;
        ui->widgetRT->setWindowFlags(Qt::Window | Qt::CustomizeWindowHint | Qt::WindowTitleHint | Qt::WindowMinimizeButtonHint);
        ui->widgetRT->showMaximized();
    }
}

void MainWindow::setLimitsY1()
{
    double lower = ui->lineEditLLimitY1->text().toDouble();
    double upper = ui->lineEditULimitY1->text().toDouble();

    ui->cromePlot->setLimitsY1(lower, upper);
}

void MainWindow::setLimitsY2()
{
    double lower = ui->lineEditLLimitY2->text().toDouble();
    double upper = ui->lineEditULimitY2->text().toDouble();

    ui->cromePlot->setLimitsY2(lower, upper);
}

void MainWindow::setLimitsY1RT()
{
    double lower = ui->lineEditLLimitY1RT->text().toDouble();
    double upper = ui->lineEditULimitY1RT->text().toDouble();

    ui->cromePlotRT->setLimitsY1(lower, upper);
}

void MainWindow::setLimitsY2RT()
{
    double lower = ui->lineEditLLimitY2RT->text().toDouble();
    double upper = ui->lineEditULimitY2RT->text().toDouble();

    ui->cromePlotRT->setLimitsY2(lower, upper);
}

void MainWindow::messageStdOut(QString stdMsg)
{
    sw.logMessage(stdMsg);
    lw.logOut(stdMsg);
}

void MainWindow::messageStdErr(QString errMsg)
{
    sw.logMessage(errMsg);
    lw.logErr(errMsg);
}

void MainWindow::loadSettings()
{
    QSettings settings(configFile, QSettings::NativeFormat);
    QString user        = settings.value("user", "root").toString();
    QString pass        = settings.value("pass", "root").toString();
    QString ip          = settings.value("ip", "127.0.0.1").toString();
    QString port1          = settings.value("port1", "8888").toString();
    QString port2          = settings.value("port2", "8889").toString();
    QString cromePath   = settings.value("cromepath", "/tmp/").toString();
    QString decimation  = settings.value("decimation", "10").toString();

    lastWorkdir     = settings.value("workdir", "").toString();

    ui->lineEditUser->setText(user);
    ui->lineEditPassword->setText(pass);
    ui->lineEditAddress->setText(ip);
    ui->lineEditPort1->setText(port1);
    ui->lineEditPort2->setText(port2);
    ui->lineEditPath->setText(cromePath);
    ui->spinDecimation->setValue(decimation.toDouble());
}

void MainWindow::saveSettings()
{
    QSettings settings(configFile, QSettings::NativeFormat);
    bool writable = settings.isWritable();

    if(writable) {
        settings.setValue("user", ui->lineEditUser->text());
        settings.setValue("pass", ui->lineEditPassword->text());
        settings.setValue("ip", ui->lineEditAddress->text());
        settings.setValue("port1", ui->lineEditPort1->text());
        settings.setValue("port2", ui->lineEditPort2->text());
        settings.setValue("cromepath", ui->lineEditPath->text());
        settings.setValue("workdir", ui->lineEditCurrent->text());
        settings.setValue("decimation", QString::number(ui->spinDecimation->value()));
    }
}
