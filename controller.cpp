#include "controller.h"
#include <QDateTime>
#include <QDir>
#include <QTextStream>

Controller::Controller()
{
    romulus_init("VULCAN");

    fileCounter = 0;
    numberOfFiles = 0;

    currentDir = "";
    workingDir = "/tmp/working";
    dest = "";

    scp = new Wrapper("sshpass");
    connect(scp, &Wrapper::wrapperNotRunning, this, &Controller::scpFinished);
    connect(scp, &Wrapper::stdoutMsg, this, &Controller::readScpOutput);
    connect(scp, &Wrapper::stderrMsg, this, &Controller::readScpOutput);

    connect(scp, &Wrapper::stdoutMsg, this, &Controller::passStdOut);
    connect(scp, &Wrapper::stderrMsg, this, &Controller::passStdErr);
}

bool Controller::isInSelectedRange(quint64 firstSelected, quint64 lastSelected, quint64 chunkBegin, quint64 chunkEnd)
{
    quint64 fromMax = 0;
    quint64 toMin = 0;

    if(firstSelected > chunkBegin) {
        fromMax = firstSelected;
    }
    else {
        fromMax = chunkBegin;
    }

    if(lastSelected < chunkEnd) {
        toMin = lastSelected;
    }
    else {
        toMin = chunkEnd;
    }


    //if fromMax is smaller or equal toMin, we have intersection
    return (fromMax <= toMin);
}

void Controller::runScp(QMap<QString, QString> argmap)
{
    fileCounter = 0;
    numberOfFiles = 0;

    //sshpass -p password ssh -o ConnectTimeout=5 user@ip 'cd /tmp; ls -1 | wc -l'
    //sshpass -p root rsync -r -v s
    //sshpass -p 'root' scp -r root@192.168.33.2:/tmp/ cromedata
    dest = QDateTime::currentDateTime().toString();
    dest = dest.replace(" ","").replace(":","");
    dest = "/tmp/"+dest;

    QStringList arguments;

    arguments.append("-p");
    arguments.append(argmap["password"]);

    arguments.append("rsync");
    arguments.append("-r");
    arguments.append("-v");
    arguments.append("--progress");
    arguments.append("--stats");
    arguments.append("-e");

    //options to avoid strict check for an automated process
    //to not require user interaction in typing "yes/no"
    arguments.append("ssh -o ConnectTimeout=10 -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null");

    arguments.append(argmap["user"]+"@"+argmap["address"]+":"+argmap["path"]+"data/hist*");
    arguments.append(argmap["user"]+"@"+argmap["address"]+":"+argmap["path"]+"events/hist*");
    arguments.append(dest.replace(" ",""));

    scp->start(arguments);
    lastFilesRemained = std::numeric_limits<quint64>::max();
    emit signalSendProgress(0);
}

void Controller::scpFinished()
{
    int code = scp->getExitCode();
    if(code == 0)
    {
        currentDir = dest;
        loadDirectory(currentDir);
    }

    emit signalScpFinished(code, currentDir);
}

void Controller::readScpOutput(QString message)
{
    QString regexString = "([0-9]+[/][0-9]+)";
    QRegExp regex = QRegExp(regexString);

    int pos = regex.indexIn(message);

    if(pos != -1) {
        QStringList records = regex.capturedTexts();

        QString singleRecord = records.last();

        QStringList values = singleRecord.split("/");

        bool result1, result2;

        numberOfFiles = values.last().toLongLong(&result2);

        quint64 remained = values.first().toLongLong(&result1);

        fileCounter = numberOfFiles - remained;


        if(result1 == true && result2 == true && lastFilesRemained > remained) {
            emit signalSendProgress(fileCounter*100/numberOfFiles);
        }
    }

    regexString = "(Number of regular files transferred: [0-9]*)";
    regex = QRegExp(regexString);

    pos = regex.indexIn(message);

    if(pos != -1) {

        QStringList records = regex.capturedTexts();

        QString singleRecord = records.last();

        QStringList values = singleRecord.split(":");

        bool resultFinish;
        quint64 allTransferedFiles = values.last().toLongLong(&resultFinish);

        if(resultFinish == true) {
            emit signalSendProgress(allTransferedFiles*100/numberOfFiles);
        }

    }


}

void Controller::registerRealtimeChunk(MeasObject object)
{
    realtimeMeasObjects.append(object);
}

QPair<quint64, quint64> Controller::requestAvailableTimestamps(QString ip, int port, Hist_Type type)
{
    QPair<quint64, quint64> timestamps;
    timestamps.first = 0;
    timestamps.second = 0;

    romulus_socket_t sock = -1;

    quint16 sequenceID = 0;

    sock = romulus_net_connect(ip.toStdString().c_str(), port);

    if(sock == -1) {
        fprintf(stderr, "Failed connection to server");
        return timestamps;
    }

    romulus_frame_t *sendFramePtr = NULL;
    romulus_hist_data_t request;

    if(type == Hist_Type::Hist_Data) {
        sendFramePtr =
                romulus_frame_create_request(romulus_frame_generate_sequence_id(&sequenceID),
                                         ROMULUS_COMMAND_CODE_HIST_TIMESTAMPS_DATA_REQUEST);
    }
    else {
        sendFramePtr =
                romulus_frame_create_request(romulus_frame_generate_sequence_id(&sequenceID),
                                         ROMULUS_COMMAND_CODE_HIST_TIMESTAMPS_EVENTS_REQUEST);
    }


    romulus_net_send(sendFramePtr, romulus_frame_get_size(sendFramePtr), sock, true);
    romulus_frame_free(sendFramePtr);
    sendFramePtr = NULL;

    romulus_frame_t *responseFrame = NULL;
    bool socketError = false;

    if ((responseFrame = romulus_net_recv(NULL, sock, true, &socketError)) == NULL
            || socketError)
    {
                romulus_net_close(NULL, sock);
                return timestamps;
    }

    if (romulus_frame_read_header(responseFrame)->cmd_code.command_code ==
            ROMULUS_COMMAND_CODE_HIST_TIMESTAMPS_DATA_REQUEST.command_code)
    {
        romulus_hist_ts_t responseData;

        romulus_frame_hist_timestamps_read_from_frame(responseFrame, &responseData);

        timestamps.first = responseData.FromTimestamp;
        timestamps.second = responseData.ToTimestamp;
    }
    else if (romulus_frame_read_header(responseFrame)->cmd_code.command_code ==
            ROMULUS_COMMAND_CODE_HIST_TIMESTAMPS_EVENTS_REQUEST.command_code)
    {

    }

    return timestamps;
}

void Controller::getDataTimestamps()
{
    emit signalAvailableTimestampsData(dataTS.first, dataTS.second);
}

void Controller::getEventTimestamps()
{
    emit signalAvailableTimestampsEvents(eventTS.first, eventTS.second);
}

void Controller::requestHistInfo(QString ip, int port, Hist_Type type, QPair<quint64, quint64> TS)
{

    romulus_socket_t sock = -1;

    quint16 sequenceID = 0;

    sock = romulus_net_connect(ip.toStdString().c_str(), port);

    if(sock == -1) {
        fprintf(stderr, "Failed connection to server");
        return;
    }

    romulus_frame_t *sendFramePtr = NULL;
    romulus_hist_ts_t request;

    if(type == Hist_Type::Hist_Data)
    {
        sendFramePtr =
                romulus_frame_create_request(romulus_frame_generate_sequence_id(&sequenceID),
                                             ROMULUS_COMMAND_CODE_HIST_DATA_REQUEST);
    }
    else {
        sendFramePtr =
                romulus_frame_create_request(romulus_frame_generate_sequence_id(&sequenceID),
                                             ROMULUS_COMMAND_CODE_HIST_EVENTS_REQUEST);
    }

    request.FromTimestamp = TS.first;
    request.ToTimestamp = TS.second;


    romulus_frame_hist_search_set_request(sendFramePtr, &request);

    romulus_frame_print_frame(sendFramePtr, stdout);
    romulus_net_send(sendFramePtr,
                     romulus_frame_get_size(sendFramePtr), sock, true);
    romulus_frame_free(sendFramePtr);
    sendFramePtr = NULL;

    romulus_frame_t *responseFrame = NULL;
    bool stop = false;
    bool socketError = false;

    histdataMeasObjects.clear();

    do
    {
        if ((responseFrame = romulus_net_recv(NULL, sock, true, &socketError)) == NULL
                || socketError)
        {
            fprintf(stderr, "Received bad response frame after setting parameters\n");
            romulus_net_close(NULL, sock);
                    return;
        }

        if (romulus_frame_read_header(responseFrame)->cmd_code.command_code ==
                ROMULUS_COMMAND_CODE_HIST_DONE_RESPONSE.command_code)
        {
            stop = true;
        }

        else if (romulus_frame_read_header(responseFrame)->cmd_code.command_code ==
                 ROMULUS_COMMAND_CODE_HIST_DATA_RESPONSE.command_code)
        {
            romulus_hist_data_t responseData;
            romulus_frame_hist_search_read_from_frame(responseFrame, &responseData);

            long long int ts = (long long int)responseData.TimeStamp;

            addHistDataChunk(QString::number(ts), responseData);

        }
        else if (romulus_frame_read_header(responseFrame)->cmd_code.command_code ==
                 ROMULUS_COMMAND_CODE_HIST_EVENTS_RESPONSE.command_code)
        {
            romulus_hist_event_t responseData;
            romulus_frame_hist_search_read_from_frame(responseFrame, &responseData);
            printf("Received frame with timestamp @ %lli\n",
                   (long long int)responseData.TimeStamp);
        }

        romulus_frame_free(responseFrame);
        responseFrame = NULL;
    } while (!stop);


    romulus_net_close(NULL, sock);
}

void Controller::requestAllHistData(QString ip, int port)
{
    QPair<quint64, quint64> TS = requestAvailableTimestamps(ip, port, Hist_Type::Hist_Data);
    requestHistInfo(ip, port, Hist_Type::Hist_Data, TS);
}

void Controller::requestAllHistEvents(QString ip, int port)
{
    QPair<quint64, quint64> TS = requestAvailableTimestamps(ip, port, Hist_Type::Hist_Event);
    requestHistInfo(ip, port, Hist_Type::Hist_Event, TS);
}

void Controller::loadDirectory(QString directory)
{
    currentDir = directory;
    QDir dir(directory);
    QStringList filtersData, filtersEvent;
    filtersData << "hist_data*";
    filtersEvent << "hist_event*";

    QStringList filesData = dir.entryList(filtersData, QDir::Files);
    QStringList filesEvent = dir.entryList(filtersEvent, QDir::Files);

    dataTS = extractTimestamps(filesData);
    eventTS = extractTimestamps(filesEvent);

    emit signalAvailableTimestampsData(dataTS.first, dataTS.second);
}

QPair<CromeDateTime, CromeDateTime> Controller::extractTimestamps(QStringList files)
{
    CromeDateTime firstDatetime = 0;
    CromeDateTime lastDatetime = 0;

    if(!files.isEmpty())
    {
        QStringList firstFileData = files.first().split("_");
        QStringList lastFileData =  files.last().split("_");

        QString firstTimestampString = firstFileData[2].remove(0,1);
        QString lastTimestampString  = lastFileData[3].remove(0,1);

        bool bStatus = false;
        quint64 firstTimestamp = firstTimestampString.toULongLong(&bStatus, 16);
        quint64 lastTimestamp  =  lastTimestampString.toULongLong(&bStatus, 16);

        firstDatetime = CromeDateTime(firstTimestamp);
        lastDatetime = CromeDateTime(lastTimestamp);

    }

    QPair<CromeDateTime, CromeDateTime> timestamps;
    timestamps.first = firstDatetime;
    timestamps.second = lastDatetime;

    return timestamps;
}

QStringList Controller::pickFiles(quint64 firstSelected, quint64 lastSelected, QStringList filters)
{
    QDir dir(currentDir);

    QStringList files = dir.entryList(filters, QDir::Files);
    bool bStatus = false;

    QStringList selectedChunks;
    bool correct = false;

    foreach(QString file, files)
    {
        QStringList fileData = file.split("_");

        QString chunkBeginString = fileData[2].remove(0,1);
        QString chunkEndString  = fileData[3].remove(0,1);

        quint64 chunkBegin =  chunkBeginString.toULongLong(&bStatus, 16);
        quint64 chunkEnd  =   chunkEndString.toULongLong(&bStatus, 16);

        correct = isInSelectedRange(firstSelected, lastSelected, chunkBegin, chunkEnd);

        if(correct) {
            selectedChunks.append(file);
        }

    }

    cleanWorkDir();

    foreach(QString file, selectedChunks)
    {
        bool result = QFile::copy(currentDir+"/"+file, workingDir+"/"+file);
    }

    return selectedChunks;
}

void Controller::saveCSVtoFile(QString filename, QString content)
{
    QFile file(filename);

    if (file.open(QIODevice::ReadWrite)) {
        QTextStream stream(&file);
        stream << content;
    }
    file.close();
}

int Controller::saveHistDataRecordToFile(FILE *output, romulus_hist_data_t& object)
{
    int n = 1;
    int elements = fread(&object, sizeof(romulus_hist_data_t), n, output);

    if(elements == n) return 0;
    else              return 1;
}

int Controller::readHistDataRecordFromFile(FILE *input, romulus_hist_data_t& object)
{
    int n = 1;
    int elements = fread(&object, sizeof(romulus_hist_data_t), n, input);

    if(elements == n) return 0;
    else              return 1;
}

int Controller::saveHistEventRecordToFile(FILE *output, romulus_hist_event_t& object)
{
    int n = 1;
    int elements = fread(&object, sizeof(romulus_hist_event_t), n, output);

    if(elements == n) return 0;
    else              return 1;
}

int Controller::readHistEventRecordFromFile(FILE *input, romulus_hist_event_t& object)
{
    int n = 1;
    int elements = fread(&object, sizeof(romulus_hist_event_t), n, input);

    if(elements == n) return 0;
    else              return 1;
}



void Controller::getRequestedData(quint64 firstSelected, quint64 lastSelected, quint64 decimation)
{
    QStringList filters;
    filters << "hist_data*";

    QStringList datafiles = pickFiles(firstSelected, lastSelected, filters);
    readDataBinaryFiles(datafiles, decimation);
}

void Controller::getRequestedEvents(quint64 firstSelected, quint64 lastSelected, quint64 decimation)
{
    QStringList filters;
    filters << "hist_event*";

    QStringList datafiles = pickFiles(firstSelected, lastSelected, filters);
    readEventsBinaryFiles(datafiles, decimation);
}

void Controller::prepareCSV(quint64 firstSelected, quint64 lastSelected, quint64 decimation, QString filename)
{
    QStringList filters;
    filters << "hist_data*";

    QStringList datafiles = pickFiles(firstSelected, lastSelected, filters);

    histdataMeasObjects.clear();
    histdataRecords.clear();

    readHistData(datafiles, decimation);

    QString csvRecords = histDataToCSV();
    saveCSVtoFile(filename, csvRecords);

    emit signalCSVReady();
}

void Controller::prepareRealtimeCSV(QString filename)
{
    QString csvRecordsRealtime = realtimeToCSV();
    saveCSVtoFile(filename, csvRecordsRealtime);

    emit signalCSVReady();
}



QString Controller::histDataToCSV()
{
    QStringList csvRecords;
    csvRecords.append(MeasObject::stringStructureHist());
    if(histdataMeasObjects.isEmpty() == false) {


        foreach(MeasObject singleObject, histdataMeasObjects) {
            csvRecords.append(singleObject.toStringHist());
        }
    }

    return csvRecords.join("\n");
}

QString Controller::realtimeToCSV()
{
    QStringList csvRecords;
    csvRecords.append(MeasObject::stringStructureRealtime());
    if(realtimeMeasObjects.isEmpty() == false) {
        foreach(MeasObject singleObject, realtimeMeasObjects) {
            csvRecords.append(singleObject.toStringRealtime());
        }
    }

    return csvRecords.join("\n");
}

void Controller::cleanWorkDir()
{
    QDir dir(workingDir);
    dir.setNameFilters(QStringList() << "histdataevent*");
    dir.setFilter(QDir::Files);
    foreach(QString dirFile, dir.entryList())
    {
        dir.remove(dirFile);
    }

    if(!QDir(workingDir).exists()) {
        QDir().mkdir(workingDir);
    }
}



void Controller::readHistData(QStringList datafiles, quint64 decimation)
{
    romulus_hist_data_t histdata;
    int sample_counter = 0;
    foreach(QString file, datafiles)
    {
        QString filepath = workingDir+"/"+file;

        FILE* datafile = fopen(filepath.toStdString().c_str(), "rb");


        int result = 0;
        while(result == 0 && datafile != NULL)
        {
            result = readHistDataRecordFromFile(datafile, histdata);
            char timestamp_buff[25];
            romulus_timestamp_str(histdata.TimeStamp, timestamp_buff);
            QString qtimestamp(timestamp_buff);

            if(sample_counter%decimation == 0)
                addHistDataChunk(qtimestamp, histdata);

            ++sample_counter;
        }

        fclose(datafile);
    }

#ifdef OLD_FILES
    char* histdataevent_folder = "/tmp/working";

    const int buff_size = 1 << 16;
    uint8_t* buff = (uint8_t*)malloc(buff_size);

    romulus_hist_search_query_t query;
    romulus_hist_search_query_init(&query);

    romulus_hist_search_result_t* qres;
    romulus_histdataevent_search(&query, histdataevent_folder, &qres);

    int histdata_count = 0;
    int fetched_size;

    quint64 counter = 0;

    while (romulus_hist_search_fetch(qres, buff, buff_size, &fetched_size) == ROMULUS_HIST_ERR_OK && qres->more) {
        int offset = 0;
        while (offset < fetched_size) {
            romulus_frame_t* frame = buff + offset;

            if (_romulus_frame_get_header(frame)->cmd_code == ROMULUS_CMD_HistDataResponseFrame && counter%decimation==0) {
                romulus_hist_data_t histdata;
                histdata_count++;
                //printf("ROMULUS_CMD_GetHistDataResponse #%d:\n", histdata_count);
                _romulus_dp_histdata_resp_load(&histdata, romulus_frame_get_data_with_offset(frame, 1));
                char timestamp_buff[25];
                romulus_timestamp_str(histdata.TimeStamp, timestamp_buff);
                QString qtimestamp(timestamp_buff);

                addHistDataChunk(qtimestamp, histdata);
            }
            offset += romulus_frame_getfullsize(frame);
            ++counter;
        }
    }
#endif
}

void Controller::readHistEvents(QStringList eventfiles, quint64 decimation)
{

    romulus_hist_event_t histevent;
    int sample_counter = 0;
    foreach(QString file, eventfiles)
    {
        QString filepath = workingDir+"/"+file;

        FILE* eventsfile = fopen(filepath.toStdString().c_str(), "rb");


        int result = 0;
        while(result == 0 && eventsfile != NULL)
        {
            result = readHistEventRecordFromFile(eventsfile, histevent);
            char timestamp_buff[25];
            romulus_timestamp_str(histevent.TimeStamp, timestamp_buff);
            QString qtimestamp(timestamp_buff);

            if(sample_counter%decimation == 0)
                addHistEventChunk(qtimestamp,histevent);

            ++sample_counter;
        }

        fclose(eventsfile);
    }

#ifdef OLD_FILES
    char* histdataevent_folder = "/tmp/working";
    const int buff_size = 1 << 16;
    uint8_t* buff = (uint8_t*)malloc(buff_size);

    romulus_hist_search_query_t query;
    romulus_hist_search_query_init(&query);

    romulus_hist_search_result_t* qres;
    romulus_histdataevent_search(&query, histdataevent_folder, &qres);

    int histevent_count = 0;
    int fetched_size;

    while (romulus_hist_search_fetch(qres, buff, buff_size, &fetched_size) == ROMULUS_HIST_ERR_OK && qres->more) {
        int offset = 0;
        while (offset < fetched_size) {
            romulus_frame_t* frame = buff + offset;

            if (_romulus_frame_get_header(frame)->cmd_code == ROMULUS_CMD_HistEventResponseFrame) {
                romulus_hist_event_t histevent;
                histevent_count++;

                _romulus_dp_histevent_resp_load(&histevent, romulus_frame_get_data_with_offset(frame, 1));
                char timestamp_buff[25];
                romulus_timestamp_str(histevent.TimeStamp, timestamp_buff);
                QString qtimestamp(timestamp_buff);

                addHistEventChunk(qtimestamp, histevent);


            }
            offset += romulus_frame_getfullsize(frame);
        }
    }
#endif
}


void Controller::readDataBinaryFiles(QStringList datafiles, quint64 decimation)
{
    histdataMeasObjects.clear();
    histdataRecords.clear();

    readHistData(datafiles, decimation);

    emit signalReadingFinished(histdataMeasObjects, histdataRecords, histeventRecords);
}

void Controller::readEventsBinaryFiles(QStringList datafiles, quint64 decimation)
{
    histeventRecords.clear();

    readHistEvents(datafiles, decimation);

    emit signalReadingFinished(histdataMeasObjects, histdataRecords, histeventRecords);
}

void Controller::addHistDataChunk(QString timestamp, romulus_hist_data_t record)
{
    QPair<QString, romulus_hist_data_t> newHistData;
    newHistData.first = timestamp;
    newHistData.second = record;

    MeasObject dataObject;
    dataObject.assignHistData(newHistData);
    histdataMeasObjects.append(dataObject);
    histdataRecords.append(newHistData);
}

void Controller::addHistEventChunk(QString timestamp, romulus_hist_event_t record)
{
    QPair<QString, romulus_hist_event_t> newHistEvent;
    newHistEvent.first = timestamp;
    newHistEvent.second = record;

    histeventRecords.append(newHistEvent);
}

void Controller::passStdOut(QString msgOut)
{
    emit signalStdOut(msgOut);
}

void Controller::passStdErr(QString msgErr)
{
    emit signalStdErr(msgErr);

}
