#include "cromeplot.h"
#include "cromedatetime.h"


enum plotHist{
    AVG = 0,
    VALUE,
    INT1,
    INT2,
    MIN,
    MAX,
    TEMP,
    HUMID,
    VOLTG,
};

enum plotMeas{
    RAWm = 0,
    BASEm,
    AVGm,
    TEMP1m,
    TEMP2m,
    TEMPCm,
    HUMIDm,
    MINm,
    MAXm,
    INT1m,
    INT2m,
};

CromePlot::CromePlot(QWidget *parent)
{
    setWindowNow();
}

void CromePlot::setupHistdataPlot()
{
    switches.insert(AVG, false);
    switches.insert(VALUE, false);
    switches.insert(INT1, false);
    switches.insert(INT2, false);
    switches.insert(MIN, false);
    switches.insert(MAX, false);
    switches.insert(TEMP, false);
    switches.insert(HUMID, false);
    switches.insert(VOLTG, false);

    plotColor.insert(AVG, QColor(255, 0, 0));
    plotColor.insert(VALUE, QColor(0, 255, 0));
    plotColor.insert(INT1, QColor(255, 0, 0));
    plotColor.insert(INT2, QColor(0, 255, 0));
    plotColor.insert(MIN, QColor(255, 0, 0));
    plotColor.insert(MAX, QColor(0, 255, 0));
    plotColor.insert(TEMP, QColor(255, 0, 0));
    plotColor.insert(HUMID, QColor(0, 0, 255));
    plotColor.insert(VOLTG, QColor(200, 100, 255));


    addGraph (xAxis, yAxis); // avg value red
    setGraph(AVG, QCPScatterStyle(QCPScatterStyle::ssPlus, 5), "Value AVG");

    addGraph(xAxis, yAxis); //  value, green
    setGraph(VALUE, QCPScatterStyle(QCPScatterStyle::ssPlus, 5), "Value");

    addGraph(xAxis, yAxis); // int1 value, blue
    setGraph(INT1, QCPScatterStyle(QCPScatterStyle::ssDisc, 5), "Integral1");

    addGraph(xAxis, yAxis); // temp1 value, red
    setGraph(INT2, QCPScatterStyle(QCPScatterStyle::ssDisc, 5), "Integral2");

    addGraph(xAxis, yAxis); // temp2 value, green
    setGraph(MIN, QCPScatterStyle(QCPScatterStyle::ssCross, 5), "MIN");

    addGraph(xAxis, yAxis); // tempcase value, blue
    setGraph(MAX, QCPScatterStyle(QCPScatterStyle::ssCross, 5), "MAX");

    addGraph(xAxis, yAxis2); // humidity value, blue
    setGraph(TEMP, QCPScatterStyle(QCPScatterStyle::ssSquare, 5), "Temperature");

    addGraph(xAxis, yAxis2); // min value, blue
    setGraph(HUMID, QCPScatterStyle(QCPScatterStyle::ssSquare, 5), "Humidity");

    addGraph (xAxis, yAxis2); // max value
    setGraph(VOLTG, QCPScatterStyle(QCPScatterStyle::ssSquare, 5), "Voltage");
}

void CromePlot::setupRtmeasPlot()
{
    switches.insert(RAWm, false);
    switches.insert(BASEm, false);
    switches.insert(AVGm, false);
    switches.insert(TEMP1m, false);
    switches.insert(TEMP2m, false);
    switches.insert(TEMPCm, false);
    switches.insert(HUMIDm, false);
    switches.insert(INT1m, false);
    switches.insert(INT2m, false);

    plotColor.insert(RAWm, QColor(255, 0, 0));
    plotColor.insert(BASEm, QColor(0, 255, 0));
    plotColor.insert(AVGm, QColor(0, 0, 255));
    plotColor.insert(TEMP1m, QColor(255, 0, 0));
    plotColor.insert(TEMP2m, QColor(0, 255, 0));
    plotColor.insert(TEMPCm, QColor(0, 0, 255));
    plotColor.insert(HUMIDm, QColor(0, 0, 255));
    plotColor.insert(MINm, QColor(255, 0, 0));
    plotColor.insert(MAXm, QColor(0, 255, 0));
    plotColor.insert(INT1m, QColor(255, 0, 255));
    plotColor.insert(INT2m, QColor(0, 255, 255));
    addGraph (xAxis, yAxis); // raw value
    setGraph(RAWm, QCPScatterStyle(QCPScatterStyle::ssPlus, 5), "ValueRaw");

    addGraph(xAxis, yAxis); // base value, green
    setGraph(BASEm, QCPScatterStyle(QCPScatterStyle::ssPlus, 5), "ValueBase");

    addGraph(xAxis, yAxis); // avg value, blue
    setGraph(AVGm, QCPScatterStyle(QCPScatterStyle::ssPlus, 5), "ValueAVG");

    addGraph(xAxis, yAxis2); // temp1 value, red
    setGraph(TEMP1m, QCPScatterStyle(QCPScatterStyle::ssDisc, 5), "Temperature1");

    addGraph(xAxis, yAxis2); // temp2 value, green
    setGraph(TEMP2m, QCPScatterStyle(QCPScatterStyle::ssDisc, 5), "Temperature2");

    addGraph(xAxis, yAxis2); // tempcase value, blue
    setGraph(TEMPCm, QCPScatterStyle(QCPScatterStyle::ssDisc, 5), "TemperatureCase");

    addGraph(xAxis, yAxis2); // humidity value, blue
    setGraph(HUMIDm, QCPScatterStyle(QCPScatterStyle::ssSquare, 5), "Humidity");

    addGraph(xAxis, yAxis); // integral1 value, blue
    setGraph(MINm, QCPScatterStyle(QCPScatterStyle::ssCross, 5), "MinValue");

    addGraph (xAxis, yAxis); // max value
    setGraph(MAXm, QCPScatterStyle(QCPScatterStyle::ssCross, 5), "MaxValue");

    addGraph (xAxis, yAxis); // max value
    setGraph(INT1m, QCPScatterStyle(QCPScatterStyle::ssCross, 5), "Integral1");

    addGraph (xAxis, yAxis); // max value
    setGraph(INT2m, QCPScatterStyle(QCPScatterStyle::ssCross, 5), "Integral2");
}

void CromePlot::setPlotType(const quint8 &isRealtime = 0)
{
    plotType = isRealtime;
    switches.clear();
    plotColor.clear();

    if(plotType == 0) {
        setupHistdataPlot();
    }
    else {
        setupRtmeasPlot();
    }
    setupPlot();
    rescaleOriginal();
}



void CromePlot::updatePlotHist(QVector<MeasObject> allRecords)
{
    QVector<double> time, avg, value, int1, int2, min, max, temp, humid, voltg;

    firstStamp = allRecords.first().getTimestamp();
    setWindow(firstStamp);
    rescaleOriginal();

    foreach(MeasObject record, allRecords) {
        time.append(record.getTimestamp()/1000.0);

        avg.append(record.getValueRaw());
        value.append(record.getValueBase());
        int1.append(record.getValueAvg());
        int2.append(record.getTemperature1());
        min.append(record.getTemperature2());
        max.append(record.getTemperatureCase());
        temp.append(record.getHumidity());
        humid.append(record.getHighVoltage());
        voltg.append(record.getMinuteValue());
    }

    graph(AVG)->setData(time,avg);
    graph(VALUE)->setData(time,value);
    graph(INT1)->setData(time,int1);
    graph(INT2)->setData(time,int2);
    graph(MIN)->setData(time,min);
    graph(MAX)->setData(time,max);
    graph(TEMP)->setData(time,temp);
    graph(HUMID)->setData(time,humid);
    graph(VOLTG)->setData(time,voltg);

    checkPlotsVisibility(switches);
    rescaleOriginal();

}

void CromePlot::setupPlot()
{
    setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectAxes);
    this->axisRect()->setRangeZoom(Qt::Vertical);
    xAxis2->setSelectableParts(NULL);

    connect(this, &CromePlot::selectionChangedByUser, this, &CromePlot::checkSelection);

    setLocale(QLocale(QLocale::English, QLocale::UnitedKingdom)); // period as decimal separator and comma as thousand separator
    legend->setVisible(false);
    QFont legendFont = font();  // start out with MainWindow's font..
    legendFont.setPointSize(9); // and make a bit smaller for legend
    legend->setFont(legendFont);
    legend->setBrush(QBrush(QColor(255,255,255,230)));
    // by default, the legend is in the inset layout of the main axis rect. So this is how we access it to change legend placement:
    this->axisRect()->insetLayout()->setInsetAlignment(0, Qt::AlignBottom|Qt::AlignLeft);

    QCPAxisRect *wideAxisRect = new QCPAxisRect(this);
    wideAxisRect->setupFullAxesBox(true);
    wideAxisRect->axis(QCPAxis::atRight, 0)->setTickLabels(true);
    wideAxisRect->addAxis(QCPAxis::atLeft)->setTickLabelColor  (QColor (  0,255,  0));


    // set ranges appropriate to show data:
    QSharedPointer<QCPAxisTickerDateTime> dateTicker(new QCPAxisTickerDateTime);
    dateTicker->setDateTimeFormat("hh:mm:ss\n d. MMMM yyyy");
    dateTicker->setDateTimeSpec(Qt::UTC);
    xAxis->setTicker(dateTicker);
    xAxis->setRange(0, 60);

    QSharedPointer<QCPAxisTickerDateTime> emptyTicker(new QCPAxisTickerDateTime);
    emptyTicker->setDateTimeFormat("");
    xAxis2->setTicker(emptyTicker);

    yAxis->setRange(-1e-20, 1e-20);
    yAxis2->setVisible(true);
    xAxis2->setVisible(false);

    yAxis2->setRange(-10.0, 100.0);
    yAxis2->setLabel(QString("Temperature [%1C] / Humidity [%] / Voltage [V]").arg(QChar(0260)));

    // make ticks on bottom axis go outward:
    xAxis->setTickLength(0, 5);
    xAxis->setSubTickLength(0, 3);

    // make ticks on right axis go inward and outward:
    yAxis2->setTickLength(3, 3);
    yAxis2->setSubTickLength(1, 1);

    // activate top and right axes, which are invisible by default:
    xAxis->setVisible(true);
    xAxis2->setVisible(true);
    yAxis->setVisible(true);
    yAxis2->setVisible(true);


}

void CromePlot::checkPlotsVisibility(QMap<int, bool> inputSwitches)
{
    for(int i = 0; i < graphCount(); ++i) {
        switches[i] = inputSwitches[i];
        graph(i)->setVisible(switches[i]);

    }

    if(switches[VOLTG] && plotType==typeHistPlot)
    {
        yAxis2->setRange(-2000.0, 100.0);
        yAxis2->setLabel(QString("Temperature [%1C] / Humidity [%] / Voltage [V]"));
    }
    else
    {
        yAxis2->setRange(-10.0, 100.0);
        yAxis2->setLabel(QString("Temperature [%1C] / Humidity [%]").arg(QChar(0260)));
    }


    replot();
}

void CromePlot::updatePlotMeas(double key, MeasObject measurements)
{
    graph(RAWm)->addData(key, measurements.getValueRaw());
    graph(BASEm)->addData(key, measurements.getValueBase());
    graph(AVGm)->addData(key, measurements.getValueAvg());
    graph(TEMP1m)->addData(key, measurements.getTemperature1());
    graph(TEMP2m)->addData(key, measurements.getTemperature2());
    graph(TEMPCm)->addData(key, measurements.getTemperatureCase());
    graph(HUMIDm)->addData(key, measurements.getHumidity());
    graph(MINm)->addData(key, measurements.getMinValue());
    graph(MAXm)->addData(key, measurements.getMaxValue());
    graph(INT1m)->addData(key, measurements.getIntegral1());
    graph(INT2m)->addData(key, measurements.getIntegral2());

}

void CromePlot::setWindowNow()
{
    quint64 timestamp = QDateTime::currentMSecsSinceEpoch();
    setWindow(timestamp);
}

void CromePlot::setWindow(quint64 timestamp)
{
    timestamp = timestamp/1000;

    quint64 window = 60*60;
    xAxis->setRange(timestamp-window/2, timestamp+window/2);
    xAxis2->setRange(timestamp-window/2, timestamp+window/2);
}

void CromePlot::rescaleOriginal()
{
//    quint64 window = 60*60;
//    xAxis->setRange(firstStamp-window/2, firstStamp+window/2);
//    xAxis2->setRange(firstStamp-window/2, firstStamp+window/2);

    xAxis->rescale(true);
    yAxis->rescale(true);

    if(switches[VOLTG] && plotType==typeHistPlot)
    {
        yAxis2->setRange(-2000.0, 100.0);
        yAxis2->setLabel(QString("Temperature [%1C] / Humidity [%] / Voltage [V]"));
    }
    else
    {
        yAxis2->setRange(-10.0, 100.0);
        yAxis2->setLabel(QString("Temperature [%1C] / Humidity [%]").arg(QChar(0260)));
    }

    replot();
}

void CromePlot::rescaleXAxis()
{
    xAxis->rescale(true);

    replot();
}

void CromePlot::rescaleYAxis()
{
    yAxis->rescale(true);

    replot();
}

void CromePlot::checkSelection()
{
    QList<QCPAxis*> axesList = selectedAxes();

    if(axesList.isEmpty())
    {
        return;
    }

    QCPAxis* selectedAxis = axesList.first();

    if(selectedAxis == yAxis)
    {
        this->axisRect()->setRangeZoom(Qt::Vertical);
        this->axisRect()->setRangeDragAxes(xAxis, yAxis);
        this->axisRect()->setRangeZoomAxes(NULL, yAxis);
    }
    else if(selectedAxis == yAxis2)
    {
        this->axisRect()->setRangeZoom(Qt::Vertical);
        this->axisRect()->setRangeDragAxes(xAxis, yAxis2);
        this->axisRect()->setRangeZoomAxes(NULL, yAxis2);
    }
    else if(selectedAxis == xAxis)
    {
        this->axisRect()->setRangeZoomAxes(xAxis, NULL);
        this->axisRect()->setRangeZoom(Qt::Horizontal);   
    }
}

void CromePlot::clearDatapoints()
{
    QVector<double> empty;

    for(int i = 0; i < this->graphCount(); ++i) {
       graph(i)->setData(empty, empty);
    }
    replot();
}

void CromePlot::showLegend(bool status)
{
    legend->setVisible(status);
    replot();
}

void CromePlot::setLimitsY1(double lower, double upper)
{
    this->yAxis->setRangeLower(lower);
    this->yAxis->setRangeUpper(upper);

    replot();
}

void CromePlot::setLimitsY2(double lower, double upper)
{
    this->yAxis2->setRangeLower(lower);
    this->yAxis2->setRangeUpper(upper);

    replot();
}

void CromePlot::setGraph(quint16 id, QCPScatterStyle ss, QString name)
{
    graph(id)->setPen(QPen(plotColor[id]));
    graph(id)->setLineStyle(QCPGraph::lsLine);
    graph(id)->setScatterStyle(ss);
    graph(id)->setName(name);
}

void CromePlot::realtimeDataSlot(MeasObject measurements)
{
    // time elapsed since start of demo, in seconds
    double key = measurements.getTimestamp()/1000.0;

    updatePlotMeas(key, measurements);
    replot();
}
