#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <cstdio>
#include <QObject>
#include "wrapper.h"
#include <QMap>
#include <QPair>
#include <QVector>
#include "cromedatetime.h"
#include "measobject.h"

extern "C" {
    #include <romulus.h>
    #include <romulus_internals.h>
    #include <romulus_frame.h>
    #include <romulus_net.h>

    #include <fcntl.h>
    #include <unistd.h>

    #include <errno.h>
    #include <string.h>
}

class Controller : public QObject
{

Q_OBJECT
public:
    Controller();

    enum class Hist_Type
    {
        Hist_Data,
        Hist_Event
    };

private:
    quint64 progress;
    quint64 fileCounter;
    quint64 numberOfFiles;
    quint64 lastFilesRemained;
    QPair<CromeDateTime, CromeDateTime> dataTS, eventTS;

    static const quint64 recordsPerFile = 64;

    Wrapper* scp;
    QString currentDir;
    QString workingDir;
    QString dest;
    QVector<MeasObject> histdataMeasObjects;
    QVector<MeasObject> realtimeMeasObjects;

    QVector<QPair<QString, romulus_hist_data_t>> histdataRecords;
    QVector<QPair<QString, romulus_hist_event_t>> histeventRecords;

    bool isInSelectedRange(quint64 firstSelectedStamp, quint64 lastSelectedStamp,
                           quint64 lowerLimit, quint64 upperLimit);


    void readHistData(QStringList, quint64);
    void readHistEvents(QStringList, quint64);
    QStringList pickFiles(quint64, quint64, QStringList);
    void saveCSVtoFile(QString, QString);

    QPair<CromeDateTime, CromeDateTime> extractTimestamps(QStringList);

    int saveHistDataRecordToFile(FILE* output, romulus_hist_data_t&);
    int readHistDataRecordFromFile(FILE* input, romulus_hist_data_t&);


    int saveHistEventRecordToFile(FILE* output, romulus_hist_event_t&);
    int readHistEventRecordFromFile(FILE* input, romulus_hist_event_t&);

signals:
    void signalScpFinished(int, QString);
    void signalAvailableTimestampsData(CromeDateTime, CromeDateTime);
    void signalAvailableTimestampsEvents(CromeDateTime, CromeDateTime);

    void signalReadingFinished(QVector<MeasObject>, QVector<QPair<QString, romulus_hist_data_t>>, QVector<QPair<QString, romulus_hist_event_t>>);
    void signalSendProgress(quint64);
    void signalCSVReady();
    void signalStdOut(QString);
    void signalStdErr(QString);


public slots:
    void runScp(QMap<QString,QString>);
    void scpFinished();
    void readScpOutput(QString);

    void registerRealtimeChunk(MeasObject);

    void requestHistInfo(QString, int, Hist_Type, QPair<quint64, quint64>);
    void requestAllHistData(QString, int);
    void requestAllHistEvents(QString, int);
    QPair<quint64,quint64> requestAvailableTimestamps(QString, int, Hist_Type);

    void getDataTimestamps();
    void getEventTimestamps();

    void loadDirectory(QString);
    void getRequestedData(quint64, quint64, quint64);
    void getRequestedEvents(quint64, quint64, quint64);
    void prepareCSV(quint64, quint64, quint64, QString);
    void prepareRealtimeCSV(QString);

    QString histDataToCSV();
    QString realtimeToCSV();

    void cleanWorkDir();
    void readDataBinaryFiles(QStringList, quint64);
    void readEventsBinaryFiles(QStringList, quint64);
    void addHistDataChunk(QString, romulus_hist_data_t);
    void addHistEventChunk(QString, romulus_hist_event_t);

    void passStdOut(QString);
    void passStdErr(QString);

};

#endif // CONTROLLER_H
