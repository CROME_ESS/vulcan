#ifndef HISTRECORDSWINDOW_H
#define HISTRECORDSWINDOW_H

#include <QWidget>

extern "C"{
    #include "romulus.h"
}

namespace Ui {
class HistrecordsWindow;
}

class HistrecordsWindow : public QWidget
{
    Q_OBJECT

public:
    explicit HistrecordsWindow(QWidget *parent = 0);
    ~HistrecordsWindow();

    void addData(QVector<QPair<QString, romulus_hist_data_t>> &);

private:
    Ui::HistrecordsWindow *ui;
    QVector<QPair<QString, romulus_hist_data_t>> data;

    void setupHistDataTable();
    void addHistDataRow(QString);
    void clearHistDataTable();

public slots:
    void loadHistDataRecord(QPair<QString, romulus_hist_data_t> record);
    void histdataSelected();
};

#endif // HISTRECORDSWINDOW_H
