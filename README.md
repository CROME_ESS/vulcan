# VULCAN

## Synopsis
VULCAN (VisUaLization for Crome ANalysis) is a toot used to interact with the CERN's ROMULUS protocol. It is a GUI application that provides the basic functionalities of the supervision. The user can review and change the CROME device parameters. In addition, the device stdout and stderr messages are captured and displayed in two separate windows for visible diagnostics.
The tool that was previously written to check the device measurements was incorporated into VULCAN using wrapper class, which captures its output and logs it in text window.

# Getting Started
## Required Dependencies

|      Name | Version  |                            Type    |
|----------:|---------:|-----------------------------------:|
|ROMULUSlib |   6.0    |                                lib |
|Qt         |   5.x    |   Qt framework (https://www.qt.io) |
|Python3    |   3.0    |                                    |
|Python3 PIP|   19.2   |      https://pip.pypa.io/en/stable |
|testcases  |   1.0    |                                Bin |


It has been built and tested successfully with gcc 4.8.5 

## Cross Compilation
compilation can be done as follows :

    $ qmake vulcan.pro
    $ make
    
The project should then build successfully. In the case of an error with qmake itself, it may be necessary to specify the qmake version:

    $ qmake -qt=qt5 vulcan.pro

## Note

As the applications, use external programs the following steps should be completed for proper use :
 Building testcases exporting the bin to $PATH,

