#-------------------------------------------------
#
# Project created by QtCreator 2018-09-12T22:45:43
#
#-------------------------------------------------

QT      += core gui network printsupport
CONFIG  += c++11


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = vulcan
TEMPLATE = app

MAIN_DIR = /usr/bin/VULCAN
INSTALLATION_DIR = $${MAIN_DIR}/app
LINK_DIR = /usr/bin
ENTRY = $${TARGET}.desktop
ENTRY_DIR = /usr/share/applications

viewer.path = /usr/bin/VULCAN/rtmeas_viewer
viewer.files += ../../rtmeas_viewer/ROMULUSRTMeasViewer

target.path = $${INSTALLATION_DIR}
target.files = $${TARGET} config.ini vulcan-icon.svg

target.commands +=  sudo ln -s $${INSTALLATION_DIR}/$${TARGET} $${LINK_DIR}; \
                    sudo cp $${ENTRY} $${ENTRY_DIR};

QMAKE_INSTALL_FILE = install -m 777

target.uninstall += sudo unlink $${LINK_DIR}/$${TARGET}; \
                    sudo rm $${ENTRY_DIR}/$${ENTRY}; \

target.depends = install_viewer

INSTALLS += viewer target


LIBS += -lpthread
LIBS += -L../../.. -lROMULUS
INCLUDEPATH += ../../../include
INCLUDEPATH += ../../../src
INCLUDEPATH += ../../../_gen
INCLUDEPATH += ../common_include
INCLUDEPATH += ../common_src

SOURCES += main.cpp\
    mainwindow.cpp \
    controller.cpp \
    cromeplot.cpp \
    histrecordswindow.cpp \
    eventrecordswindow.cpp \
    statuswindow.cpp \
    logswindow.cpp

#Define common sources
SOURCES += ../common_src/cromedatetime.cpp \
    ../common_src/measobject.cpp \
    ../common_src/stdhandler.cpp \
    ../common_src/plotdatahandler.cpp \
    ../common_src/qcustomplot.cpp \
    ../common_src/wrapper.cpp \

HEADERS  += mainwindow.h \
    controller.h \
    cromeplot.h \
    histrecordswindow.h \
    eventrecordswindow.h \
    statuswindow.h \
    logswindow.h

#Define common headers
HEADERS += ../common_include/cromedatetime.h \
    ../common_include/measobject.h \
    ../common_include/stdhandler.h \
    ../common_include/plotdatahandler.h \
    ../common_include/qcustomplot.h \
    ../common_include/wrapper.h \


FORMS    += mainwindow.ui \
    histrecordswindow.ui \
    eventrecordswindow.ui \
    statuswindow.ui \
    logswindow.ui
