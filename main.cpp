#include "mainwindow.h"
#include "plotdatahandler.h"
#include <QApplication>

#include "controller.h"
#include "stdhandler.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    MainWindow window;
    Controller controller;
    PlotDataHandler plotHandler;
    StdHandler ioLogger;


    QObject::connect(&window, &MainWindow::scpRun, &controller, &Controller::runScp);
    QObject::connect(&controller, &Controller::signalScpFinished, &window, &MainWindow::scpResult);
    QObject::connect(&controller, &Controller::signalAvailableTimestampsData, &window, &MainWindow::setDataTimestamps);
    QObject::connect(&controller, &Controller::signalAvailableTimestampsEvents, &window, &MainWindow::setEventTimestamps);

    QObject::connect(&window, &MainWindow::signalRequestDataTimestamps, &controller, &Controller::getDataTimestamps);
    QObject::connect(&window, &MainWindow::signalRequestEventTimestamps, &controller, &Controller::getEventTimestamps);

    QObject::connect(&window, &MainWindow::signalConfirmTimestampsData, &controller, &Controller::getRequestedData);
    QObject::connect(&window, &MainWindow::signalConfirmTimestampsEvent, &controller, &Controller::getRequestedEvents);
    QObject::connect(&window, &MainWindow::signalGetCSV, &controller, &Controller::prepareCSV);
    QObject::connect(&window, &MainWindow::signalGetRealtimeCSV, &controller, &Controller::prepareRealtimeCSV);
    QObject::connect(&window, &MainWindow::signalRequestDirectoryLoad, &controller, &Controller::loadDirectory);
    QObject::connect(&controller, &Controller::signalReadingFinished, &window, &MainWindow::receiveDataRecords);
    QObject::connect(&controller, &Controller::signalSendProgress, &window, &MainWindow::updateProgressBar);

    QObject::connect(&controller, &Controller::signalStdOut, &window, &MainWindow::messageStdOut);
    QObject::connect(&controller, &Controller::signalStdErr, &window, &MainWindow::messageStdErr);

    QObject::connect(&window, &MainWindow::signalStartRealtimePlot, &plotHandler, &PlotDataHandler::start);
    QObject::connect(&plotHandler, &PlotDataHandler::signalNewChunk, &window, &MainWindow::realtimeDataSlot);
    QObject::connect(&plotHandler, &PlotDataHandler::signalNewChunk, &controller, &Controller::registerRealtimeChunk);
    QObject::connect(&plotHandler, &PlotDataHandler::signalStdOut, &window, &MainWindow::messageStdOut);
    QObject::connect(&plotHandler, &PlotDataHandler::signalStdErr, &window, &MainWindow::messageStdErr);


//    ioLogger.redirectOut();
//    ioLogger.redirectErr();
    QObject::connect(&ioLogger, &StdHandler::stdOutMessage, &window, &MainWindow::messageStdOut);
    QObject::connect(&ioLogger, &StdHandler::stdErrMessage, &window, &MainWindow::messageStdErr);


    window.showMaximized();

    return a.exec();
}
