#ifndef CROMEPLOT_H
#define CROMEPLOT_H

#include <qcustomplot.h>
#include <QMap>
#include "measobject.h"

class CromePlot : public QCustomPlot
{
public:
    CromePlot(QWidget *parent = 0);

    void setPlotType(const quint8 &isRealtime);

    const static quint8 typeHistPlot = 0;
    const static quint8 typeMeasPlot = 1;

    void setupHistdataPlot();
    
    void setupRtmeasPlot();
    
private:
    quint8 plotType;

    QMap<int, bool> switches;
    QMap<int, QColor> plotColor;
    quint64 firstStamp;

    void setupPlot();
    void setGraph(quint16 id, QCPScatterStyle ss, QString name);


signals:
    void startPlotter();

public slots:
    void updatePlotHist(QVector<MeasObject>);
    void checkPlotsVisibility(QMap<int, bool>);

    void realtimeDataSlot(MeasObject);
    void updatePlotMeas(double key, MeasObject measurements);
    void setWindowNow();
    void setWindow(quint64);
    void rescaleOriginal();
    void rescaleXAxis();
    void rescaleYAxis();
    void checkSelection();
    void clearDatapoints();
    void showLegend(bool);

    void setLimitsY1(double, double);
    void setLimitsY2(double, double);
};

#endif // CROMEPLOT_H
