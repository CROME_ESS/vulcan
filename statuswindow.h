#ifndef STATUSWINDOW_H
#define STATUSWINDOW_H

#include <QWidget>
#include <QPushButton>
#include <QMap>
#include "measobject.h"

namespace Ui {
class StatusWindow;
}

class StatusWindow : public QWidget
{
    Q_OBJECT

public:
    explicit StatusWindow(QWidget *parent = 0);
    ~StatusWindow();

private:
    Ui::StatusWindow *ui;
    void setButtonColor(QPushButton*, bool);
    QMap<int, QString> mode;

public slots:
    void realtimeStatus(MeasObject);
    void logMessage(QString);
    void clearMessages();
};

#endif // STATUSWINDOW_H
