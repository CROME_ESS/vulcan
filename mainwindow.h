#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMap>
#include <QCheckBox>
#include "cromedatetime.h"
#include "measobject.h"

#include "histrecordswindow.h"
#include "eventrecordswindow.h"
#include "statuswindow.h"
#include "logswindow.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    HistrecordsWindow hw;
    EventrecordsWindow ew;
    StatusWindow sw;
    LogsWindow lw;

    QMap<int, QCheckBox*> checkboxes;
    QMap<int, QCheckBox*> checkboxesRT;
    QString configFile;
    QString lastWorkdir;

    bool detached, detachedRT;

    void setupWidgets();
    void setupSpinboxes();
    void saveSettings();
    void loadSettings();


    void addHistCheckboxes();

    void addRTCheckBoxes();

signals:
    void scpRun(QMap<QString, QString> credentials);
    void signalConfirmTimestampsData(qint64, qint64, quint64);
    void signalConfirmTimestampsEvent(qint64, qint64, quint64);

    void signalGetCSV(qint64, qint64, quint64, QString);
    void signalGetRealtimeCSV(QString);
    void signalRequestDirectoryLoad(QString);
    void signalStartRealtimePlot(QStringList);
    void signalRequestDataTimestamps();
    void signalRequestEventTimestamps();

public slots:
    void downloadFiles();
    void selectDirectory();
    void updateProgressBar(quint64);
    void setMinProgressBar(quint64);
    void setMaxProgressBar(quint64);
    void scpResult(int, QString);

    void setTimestamps(CromeDateTime, CromeDateTime);

    void setDataTimestamps(CromeDateTime, CromeDateTime);
    void setEventTimestamps(CromeDateTime, CromeDateTime);

    void confirmTimestamps();
    void prepareCSVRequest();
    void prepareRealtimeCSVRequest();
    void verifyTimestamps();
    QPair<quint64,quint64> readInputTimestamps();
    void receiveDataRecords(QVector<MeasObject>, QVector<QPair<QString, romulus_hist_data_t>>, QVector<QPair<QString, romulus_hist_event_t>>);
    void plotsSelected();
    void rescale();
    void rescaleX();
    void rescaleY();
    void rescaleRT();
    void rescaleXRT();
    void rescaleYRT();
    void startRealtimePlot();
    void clearRealtimePlot();
    void realtimeDataSlot(MeasObject);

    void manipulatePlot();
    void manipulatePlotRT();

    void setLimitsY1();
    void setLimitsY2();
    void setLimitsY1RT();
    void setLimitsY2RT();

    void messageStdOut(QString);
    void messageStdErr(QString);
};

#endif // MAINWINDOW_H
