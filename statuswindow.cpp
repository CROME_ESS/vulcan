#include "statuswindow.h"
#include "ui_statuswindow.h"

StatusWindow::StatusWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::StatusWindow)
{
    ui->setupUi(this);

    ui->groupBoxStatus->setTitle("Device Realtime status");
    ui->groupBoxStdMsg->setTitle("StdMessages");

    ui->labelInfo->setText("Please start the Realtimeplot first.");

    ui->pushButtonAlert->setText("Alert");
    ui->pushButtonAlarm->setText("Alarm");
    ui->pushButtonSpecialAlarm->setText("SpecialAlarm");

    ui->pushButtonMinorFault->setText("MinorFault");
    ui->pushButtonMajorFault->setText("MajorFault");

    ui->pushButtonMode->setText("Mode");

    ui->pushButtonSensor->setText("SensorConnection");
    ui->pushButtonPower->setText("PowerSupply");
    ui->pushButtonBattery->setText("Battery");
    ui->pushButtonCharger->setText("Charger");

    mode.insert(0, "Off");
    mode.insert(1, "Measurement");
    mode.insert(2, "Maintenance");
    mode.insert(3, "Calibration");
}

StatusWindow::~StatusWindow()
{
    delete ui;
}

void StatusWindow::setButtonColor(QPushButton *button, bool status)
{
    if(status == true) {
        button->setStyleSheet("background-color: green");
    }
    else {
        button->setStyleSheet("background-color: red");
    }

    button->update();
}

void StatusWindow::realtimeStatus(MeasObject record) {

    ui->labelInfo->hide();
    setButtonColor(ui->pushButtonAlert, record.getAlert());
    setButtonColor(ui->pushButtonAlarm, record.getAlarm());
    setButtonColor(ui->pushButtonSpecialAlarm, record.getSpecialAlarm());

    setButtonColor(ui->pushButtonMinorFault, record.getMinorFault());
    setButtonColor(ui->pushButtonMajorFault, record.getMajorFault());

    int modeNumber = record.getMode();
    setButtonColor(ui->pushButtonMode, modeNumber);
    ui->pushButtonMode->setText("Mode: [" + QString::number(modeNumber) + "] " + mode[modeNumber]);

    setButtonColor(ui->pushButtonSensor, record.getSensorConnection());
    setButtonColor(ui->pushButtonPower, record.getPowerSupply());
    setButtonColor(ui->pushButtonBattery, record.getBattery());
    setButtonColor(ui->pushButtonCharger, record.getCharger());
}

void StatusWindow::logMessage(QString msg)
{
    clearMessages();
    ui->plainTextEdit->appendPlainText(msg);
    ui->plainTextEdit->appendPlainText("\n");
}

void StatusWindow::clearMessages()
{
    ui->plainTextEdit->clear();
}
