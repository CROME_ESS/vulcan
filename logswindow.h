#ifndef LOGSWINDOW_H
#define LOGSWINDOW_H

#include <QWidget>

namespace Ui {
class logsWindow;
}

class LogsWindow : public QWidget
{
    Q_OBJECT

public:
    explicit LogsWindow(QWidget *parent = 0);
    ~LogsWindow();

private:
    Ui::logsWindow *ui;

public slots:
    void clear();
    void logOut(QString);
    void logErr(QString);
};

#endif // LOGSWINDOW_H
