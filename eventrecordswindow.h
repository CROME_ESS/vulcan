#ifndef EVENTRECORDSWINDOW_H
#define EVENTRECORDSWINDOW_H

#include <QWidget>
#include <QMap>

extern "C"{
    #include "romulus.h"
}

namespace Ui {
class EventrecordsWindow;
}

class EventrecordsWindow : public QWidget
{
    Q_OBJECT

public:
    explicit EventrecordsWindow(QWidget *parent = 0);
    ~EventrecordsWindow();

    void addEvents(QVector<QPair<QString, romulus_hist_event_t>> &);

private:
    Ui::EventrecordsWindow *ui;
    QVector<QPair<QString, romulus_hist_event_t>> events;
    QMap<int, QString> statusInfo;

    void setupHistEventTable();
    void addHistEventRow(QString);
    void clearHistEventTable();
    void setupStatusInfo();

public slots:
    void loadHistEventRecord(QPair<QString, romulus_hist_event_t>);
    void histeventSelected();

};

#endif // EVENTRECORDSWINDOW_H
