#include "logswindow.h"
#include "ui_logswindow.h"

LogsWindow::LogsWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::logsWindow)
{
    ui->setupUi(this);

    ui->labelStdOut->setText("StdOut");
    ui->labelStdErr->setText("StdErr");

    ui->pushButtonClear->setText("Clear");

    connect(ui->pushButtonClear, &QPushButton::clicked, this, &LogsWindow::clear);
}

LogsWindow::~LogsWindow()
{
    delete ui;
}

void LogsWindow::clear()
{
    ui->plainStdOut->clear();
    ui->plainStdErr->clear();
}

void LogsWindow::logOut(QString msg)
{
     ui->plainStdOut->appendPlainText(msg);
}

void LogsWindow::logErr(QString msg)
{
     ui->plainStdErr->appendPlainText(msg);
}

