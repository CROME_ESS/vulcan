#include "histrecordswindow.h"
#include "ui_histrecordswindow.h"

HistrecordsWindow::HistrecordsWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::HistrecordsWindow)
{
    ui->setupUi(this);
    setupHistDataTable();
}

HistrecordsWindow::~HistrecordsWindow()
{
    delete ui;
}

void HistrecordsWindow::addData(QVector<QPair<QString, romulus_hist_data_t>> &records)
{
    data.clear();
    clearHistDataTable();

    data = records;

    foreach (auto element, data) {
       addHistDataRow(element.first);
    }
}

void HistrecordsWindow::setupHistDataTable()
{
    ui->tableWidgetHistData->setColumnCount(1);
    ui->tableWidgetHistData->setHorizontalHeaderItem(0, new QTableWidgetItem("HistData Record"));
    ui->tableWidgetHistData->horizontalHeader()->setStretchLastSection(true);

    connect(ui->tableWidgetHistData, &QTableWidget::currentCellChanged, this, &HistrecordsWindow::histdataSelected);

}

void HistrecordsWindow::addHistDataRow(QString date)
{
    int rowCount = ui->tableWidgetHistData->rowCount();
    ui->tableWidgetHistData->insertRow(rowCount);

    const QString data = date;
    QTableWidgetItem* cell = new QTableWidgetItem;
    cell->setText(data);
    ui->tableWidgetHistData->setItem(rowCount,0,cell);
}

void HistrecordsWindow::clearHistDataTable()
{
    while (ui->tableWidgetHistData->rowCount() > 0) {
        ui->tableWidgetHistData->removeRow(0);
    }
}

void HistrecordsWindow::histdataSelected()
{
    int row = ui->tableWidgetHistData->currentRow();

    if(data.size() > row && row >=0) {
        loadHistDataRecord(data[row]);
    }
}

void HistrecordsWindow::loadHistDataRecord(QPair<QString, romulus_hist_data_t> record)
{
    QString timestamp = record.first;
    ui->lineTimestamp->setText(timestamp);

    romulus_hist_data_t data = record.second;

    ui->lineAvgValue->setText(QString::number(data.AvgValue));
    ui->lineIntegral1->setText(QString::number(data.Integral1));
    ui->lineIntegral2->setText(QString::number(data.Integral2));
    ui->lineMeasUnit->setText(QString::number(data.MeasUnit));
    ui->lineIntegralAlarms->setText(QString::number(data.IntegralAlarms));
    ui->lineMode->setText(QString::number(data.Mode));
    ui->lineAlert->setText(QString::number(data.Alert));
    ui->lineAlarm->setText(QString::number(data.Alarm));
    ui->lineSpecialAlarm->setText(QString::number(data.SpecialAlarm));
    ui->lineMinorFault->setText(QString::number(data.MinorFault));
    ui->lineMajorFault->setText(QString::number(data.MajorFault));
    ui->linePersistenFault->setText(QString::number(data.PersistentFault));
    ui->lineHighVoltage->setText(QString::number(data.HighVoltage));
    ui->lineMinValue->setText(QString::number(data.MinValue));
    ui->lineMaxValue->setText(QString::number(data.MaxValue));
    ui->lineTemperature->setText(QString::number(data.Temperature));
    ui->lineHumidity->setText(QString::number(data.Humidity));
    ui->lineMSID->setText(QString::number(data.MSID));
    ui->lineValue->setText(QString::number(data.Value));
    ui->lineMinutHourFlag->setText(QString::number(data.MinutHourFlag));

}
