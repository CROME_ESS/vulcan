#include "eventrecordswindow.h"
#include "ui_eventrecordswindow.h"

EventrecordsWindow::EventrecordsWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::EventrecordsWindow)
{
    ui->setupUi(this);
    setupStatusInfo();
    setupHistEventTable();

}

EventrecordsWindow::~EventrecordsWindow()
{
    delete ui;
}

void EventrecordsWindow::addEvents(QVector<QPair<QString, romulus_hist_event_t> > &records)
{
    events.clear();
    clearHistEventTable();

    events = records;

    foreach (auto element, events) {
       addHistEventRow(element.first);
    }
}

void EventrecordsWindow::setupHistEventTable()
{
    ui->tableWidgetHistEvents->setColumnCount(1);
    ui->tableWidgetHistEvents->setHorizontalHeaderItem(0, new QTableWidgetItem("HistEvent Record"));
    ui->tableWidgetHistEvents->horizontalHeader()->setStretchLastSection(true);

    connect(ui->tableWidgetHistEvents, &QTableWidget::currentCellChanged, this, &EventrecordsWindow::histeventSelected);
}

void EventrecordsWindow::addHistEventRow(QString date)
{
    int rowCount = ui->tableWidgetHistEvents->rowCount();
    ui->tableWidgetHistEvents->insertRow(rowCount);

    const QString data = date;
    QTableWidgetItem* cell = new QTableWidgetItem;
    cell->setText(data);
    ui->tableWidgetHistEvents->setItem(rowCount,0,cell);
}

void EventrecordsWindow::clearHistEventTable()
{
    while (ui->tableWidgetHistEvents->rowCount() > 0) {
        ui->tableWidgetHistEvents->removeRow(0);
    }
}

void EventrecordsWindow::histeventSelected()
{
    int row = ui->tableWidgetHistEvents->currentRow();

    if(events.size() > row && row >=0) {
        loadHistEventRecord(events[row]);
    }
}

void EventrecordsWindow::loadHistEventRecord(QPair<QString, romulus_hist_event_t> record)
{
    QString timestamp = record.first;
    ui->lineEventTimestamp->setText(timestamp);

    romulus_hist_event_t event = record.second;

    QString id = QString::number(event.ID, 16).toUpper();
    if(id.length() == 1) id.prepend("0");
    id.prepend("0x");
    ui->lineEventId->setText(id);
    ui->lineEventDescription->setText(statusInfo[event.ID]);
    ui->lineEventMSID->setText(QString::number(event.MSID));
    ui->lineEventMode->setText(QString::number(event.Mode));
    ui->lineEventTransition->setText(QString::number(event.Transition));
}

void EventrecordsWindow::setupStatusInfo()
{
    statusInfo.insert(0x00, "Timestamp");
    statusInfo.insert(0x01, "Alert");
    statusInfo.insert(0x02, "Alarm");
    statusInfo.insert(0x03, "SpecialAlarm");
    statusInfo.insert(0x04, "MinorFault");
    statusInfo.insert(0x05, "MajorFault");
    statusInfo.insert(0x06, "PersistentFault");
    statusInfo.insert(0x07, "TimeSyncFault");
    statusInfo.insert(0x08, "MinValueFault");
    statusInfo.insert(0x09, "MaxValueFault");
    statusInfo.insert(0x0A, "TempFault");
    statusInfo.insert(0x0B, "TempHighFault");
    statusInfo.insert(0x0C, "TempDamageFault");
    statusInfo.insert(0x0D, "HumidityFault");
    statusInfo.insert(0x0E, "PowerFault");
    statusInfo.insert(0x0F, "BatteryFault");

    statusInfo.insert(0x10, "ChargerFault");
    statusInfo.insert(0x11, "InputCurrentFault");
    statusInfo.insert(0x12, "HVnominalFault");
    statusInfo.insert(0x13, "HVFault");
    statusInfo.insert(0x14, "LVFault");
    statusInfo.insert(0x15, "Input1");
    statusInfo.insert(0x16, "Input2");
    statusInfo.insert(0x17, "Input3");
    statusInfo.insert(0x18, "Input4");
    statusInfo.insert(0x19, "Output1");
    statusInfo.insert(0x1A, "Output2");
    statusInfo.insert(0x1B, "Output3");
    statusInfo.insert(0x1C, "Output4");
    statusInfo.insert(0x1D, "TimeSync");
    statusInfo.insert(0x1E, "NetworkConnection");
    statusInfo.insert(0x1F, "SensorConnection");

    statusInfo.insert(0x20, "PowerSupply");
    statusInfo.insert(0x21, "Battery");
    statusInfo.insert(0x22, "Charger");
    statusInfo.insert(0x23, "Temperature");
    statusInfo.insert(0x24, "Humidity");
    statusInfo.insert(0x25, "CAUStatus");
    statusInfo.insert(0x26, "CAULink");
    statusInfo.insert(0x27, "CAUAMCOS");
    statusInfo.insert(0x28, "CAULinkFault");
    statusInfo.insert(0x29, "Status1");
    statusInfo.insert(0x2A, "RTMeasBufferOverflow");
    statusInfo.insert(0x2B, "HistDataBufferOverflow");
    statusInfo.insert(0x2C, "HistDataDuplicatedID");
    statusInfo.insert(0x2D, "HistDataStorageFailure");
    statusInfo.insert(0x2E, "Mode");
    statusInfo.insert(0x2F, "MemFullFault");

    statusInfo.insert(0x30, "TempCoherenceFault");
    statusInfo.insert(0x31, "IntegralAlarm1");
    statusInfo.insert(0x32, "IntegralAlarm2");

}
